// 1.   Declare a variable named "tree". Use a keyword that allows reassignment,
//      but not re-declaration
let tree
// 2.   Assign a value of 'Oak' to the tree variable
tree = "oak";
// 3.   Initialize a constant variable named "alaskaStateFlower",
//      and assign it the value of 'Forget-me-not'
const alaskaStateFlower = 'forget-me-not';
// 4.   Initialize a variable named "numberOfChoice" with any number
let numberOfChoice
numberOfChoice = 4;
// 5.   Initialize a variable named "binaryChoice" with any boolean
let binaryChoice
binaryChoice = true;

// 6.   Assign defaultVariableValue to have the string representation
//      of JavaScript's default variable value
const defaultVariableValue = 'x';

// 7.   Assign lackOfValue to be the data type that a developer should
//      assign when a value is not known ahead of time. Your answer should
//      not be a string
let lackOfValue;

lackOfValue = null;

// 8.   What would happen if you tried to re-assign alaskaStateFlower to
//      'Magnolia'? Uncomment the variable that is correct
// let reassignmentResult = 'alaskaStateFlower becomes Magnolia';
// let reassignmentResult = 'Nothing happens, and alaskaStateFlower is still Forget-me-not';
 let reassignmentResult = 'An error occurs, since you can\'t reassign a variable that was declared with const';

// 9.   Create a constant variable named operandTwo with any number
const operandTwo = 2;
// 10.  Create a variable named "product" and assign it to the value of 
//      the "numberOfChoice" variable times the "operandTwo" variable
let product = numberOfChoice*operandTwo
// 11.  Create a variable named "productOutput" and give it a string value with
//      the following sentence: "The product is: <answer_here>", ensuring that <answer_here> is your product variable
let productOutput = 'the product is: 8';
///////////////////////
// TYPE CONVERSIONS! //
// In the following section, you should identify the data type that will result when 
// the expression is evaluated. Assign variables to be one of the following values:
// ['string', 'number', 'NaN']
//  Make sure to include quotes in your answer.
///////////////////////

// 12.  3.14159 + 6.022
const dataType12 = 'number';

// 13.  -10 + '5'
const dataType13 = 'string';

// 14.  '-10' + 5
const dataType14 = 'string';

// 15.  -10 - '5'
const dataType15 = 'string';

// 16. '-10' * 5
const dataType16 = 'NaN';

// 17.  -10 / '5'
const dataType17 = 'NaN';

// 18.  -10 * 'a1'
const dataType18 = 'NaN';
